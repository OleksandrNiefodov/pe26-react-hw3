import { Route, Routes } from 'react-router-dom';
import FavouriteRoute from './routes/FavouriteRoute';
import CartRoute from './routes/CartRoute';
import ShopPage from './components/ShopPage';


const AppRouter = ({ 
    onFavClick, 
    favorites, 
    products, 
    onAddToCart, 
    selectedProduct, 
    cart, 
    onClose, 
    onRemoveFromCart,
    removeFromFav
    }) => {
        
    return (
       <Routes>
            <Route path='/' element={ <ShopPage 
                onFavClick={onFavClick}
                favorites={favorites}
                products={products}
                onAddToCart={onAddToCart}/> } />

            <Route path='favourite' element={ <FavouriteRoute 
                favorites={favorites}
                removeFromFav={removeFromFav}/> } />

            <Route path='cart' element={ <CartRoute 
                selectedProduct={ selectedProduct } 
                cart={cart} 
                onClose={onClose} 
                onRemoveFromCart={onRemoveFromCart}
            />}/>

            <Route path='*' element={ <h1 style={{ marginTop: 100 }}>404 - PAGE NOT FOUND</h1>  } />

        </Routes>
    );
}

export default AppRouter;
