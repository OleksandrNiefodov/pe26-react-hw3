import { useState, useEffect } from 'react';
import Header from './components/Header';
import Modal from './components/Modal';
import axios from 'axios';
import AppRouter from './AppRouter';
import { useImmer } from 'use-immer';
import DeleteModal from './components/deleteModal';


function App() {


  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await axios.get('database.json');
        setProducts(data);

      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  const [favorites, setFavorites] = useImmer(() => {
    const savedFavorites = localStorage.getItem('favorites');
    return savedFavorites ? JSON.parse(savedFavorites) : [];
  });

  const [cart, setCart] = useImmer(() => {
    const savedCart = localStorage.getItem('cart');
    return savedCart ? JSON.parse(savedCart) : [];
  });

  const [products, setProducts] = useState([]);
  const [isModalAddToCartOpen, setIsModalAddToCartOpen] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [showRemoveModal, setShowRemoveModal] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null)

  const handleFavoriteClick = (product) => {
    const updatedFavorites = favorites.includes(product)
      ? favorites.filter(id => id !== product)
      : [...favorites, product];
    setFavorites(updatedFavorites);
    localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
  };

const handleDeleteFavorite = (idToDelete) => {
  const updatedProducts = favorites.filter(product => product.id !== idToDelete);
  setFavorites(updatedProducts);
};



  // Add to cart -------------------------------------------
  const handleAddToCart = (product) => {
    setSelectedProduct(product);
    setIsModalAddToCartOpen(true);
  };

  const confirmAddToCart = () => {
    setCart((draft) => {
      const isProductInCart = draft.some(item => item.id === selectedProduct.id);
      if (!isProductInCart) {
        draft.push(selectedProduct);
      }
    });
    setIsModalAddToCartOpen(false);
  };

  const handleCloseModal = () => {
    setIsModalAddToCartOpen(false);
    setSelectedProduct(null);
  };

  // Delete from Cart---------------------------------------

  const handleRemoveFromCart = (product) => {
    setDeleteItem(product);
    setShowRemoveModal(true);
  };

  const confirmRemoveFromCart = () => {
    setCart((draft) => {
      const index = draft.findIndex(item => item.id === deleteItem.id);
      if (index !== -1) {
        draft.splice(index, 1);
      }
    });
    setShowRemoveModal(false);
    setDeleteItem(null);
  };

  const handleCloseDeleteModal = () => {
    setShowRemoveModal(false);
    setDeleteItem(null);
  };

  // useeffects --------------------------------------------
  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites]);


  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
  }, [cart]);

  return (

    <>
      <header>
        <Header
          favorites={favorites}
          cart={cart}
        />
      </header>

      <section>
        <AppRouter
          onFavClick={handleFavoriteClick}
          favorites={favorites}
          products={products}
          onAddToCart={handleAddToCart}
          cart={cart}
          onRemoveFromCart={handleRemoveFromCart}
          removeFromFav={handleDeleteFavorite}
        />

        <Modal
          onConfirm={confirmAddToCart}
          showModal={isModalAddToCartOpen}
          onClose={handleCloseModal}
          cart={cart}
          selectedProduct={selectedProduct}
        />

        <DeleteModal
          onConfirm={confirmRemoveFromCart}
          showModal={showRemoveModal}
          onClose={handleCloseDeleteModal}
        />
      </section>

    </>
  )
}

export default App
