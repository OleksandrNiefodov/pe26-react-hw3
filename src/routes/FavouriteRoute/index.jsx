import { useEffect, useState } from 'react';
import styles from './FavouriteRoute.module.scss';
import PropTypes from 'prop-types';


const FavouriteRoute = ({ favorites = [], removeFromFav }) => {

    const [emptyFavorite, setEmptyFavorite] = useState(false)

    useEffect(() => {
      if(favorites.length === 0) {
        setEmptyFavorite(true)
      }
    })

    return (

        <div className={styles.container}>
            <h1>Favorites</h1>
            { emptyFavorite ? <h2>There is no favorite items yet</h2> :
            <div className={styles.cards} >
                {favorites.map((product) => (
                    <div key={product.id} className={styles.card}>
                        <div>
                            <h2>{product.productName}</h2>
                            <p>Price: {product.price}</p>
                            <p>article: {product.article}</p>
                        </div>


                        <button onClick={(() => removeFromFav(product.id))}>Delete</button>
                    </div>
                ))}
            </div>}
        </div>
    );
};

FavouriteRoute.propTypes = {
    favorites: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            productName: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            article: PropTypes.string.isRequired,
        })
    ).isRequired,
};

export default FavouriteRoute;
