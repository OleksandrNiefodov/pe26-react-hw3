
import styles from './CartRoute.module.scss';
import CartItem from '../../components/CartItem';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

const CartRoute = ({
  cart = [],
  onRemoveFromCart
}) => {

  const [emptyCart, setEmptyCart] = useState(false)

  useEffect(() => {
    if(cart.length === 0) {
      setEmptyCart(true)
    }
  })

  const totalPrice = cart.reduce((acc, el) => acc + el.price, 0);

  return (
    
    
    <div className={styles.container}>
      <h1>Cart</h1>
      { emptyCart ? <h2>There is no items in cart yet</h2> :
      <div className={styles.cards}>
        {cart.map((product) => <CartItem key={product.id}
          product={product}
          onRemoveFromCart={onRemoveFromCart}
        />)}
      </div>}
      
      { !emptyCart && <h2 className={styles.totalPrice} >Total price: {totalPrice}</h2>}
    </div>
    
    
  );
};

CartRoute.propTypes = {
  cart: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      productName: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      article: PropTypes.string.isRequired,
    })
  ).isRequired,
  onRemoveFromCart: PropTypes.func.isRequired,
  handleRemoveFromCart: PropTypes.func,
};

export default CartRoute;
