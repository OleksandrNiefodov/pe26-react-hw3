import ShopPage from '../../components/ShopPage';
import styles from './HomeRoute.module.scss';

const HomeRoute = ({  }) => {
    return (
        <div>
            <ShopPage />
        </div>
    );
}

export default HomeRoute;
