import PropTypes from 'prop-types';
import RenderCard from '../RenderCard'
import styles from "./ShopPage.module.scss";
import Button from '../Button';

const ShopPage = ({ onFavClick, favorites, onAddToCart, products }) => {

  return (
    <div className={styles.cardsContainer}>

      <h1 className={styles.title}>Fruits Store</h1>

      <div className={styles.cardsWrapper}>
        {products.map((product) => (
          <RenderCard
            key={product.id}
            product={product}
            onAddToCart={onAddToCart}
            onFavClick={onFavClick}
            favorites={favorites}
          />
        ))}
      </div>

    </div>
  );
};

ShopPage.propTypes = {
  onFavClick: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(PropTypes.object).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired,
};

export default ShopPage;
