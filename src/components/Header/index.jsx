import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.module.scss';
import NavMenu from '../NavMenu';

const Header = ({ favorites = [], cart = [] }) => {

  return (
    <div className={styles.header}>
      <NavMenu 
        favorites={favorites} 
        cart={cart} />
    </div>
  );
};

Header.propTypes = {
  favorites: PropTypes.array,
  cart: PropTypes.array,
};

export default Header;