import styles from './DeleteModal.module.scss';
import PropTypes from 'prop-types';

const DeleteModal = ({ 
    onConfirm,
    showModal,
    onClose,

}) => {

    if (!showModal) {
        return null
    }

    return (
        <div className={styles.modalWindow}>

            <div className={styles.content}>
                <p>Do you really want to delete this item from the cart?</p>

                <div className={styles.buttonContainer}>

                    <button onClick={onClose}>Cancel</button>
                    <button onClick={onConfirm} className={styles.cancelBtn}>Delete!</button>

                </div>

            </div>
            <div className={styles.background} onClick={onClose} />
            
        </div>
    );
}

DeleteModal.propTypes = {
    onConfirm: PropTypes.func.isRequired,
    showModal: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default DeleteModal;

