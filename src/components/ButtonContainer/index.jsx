import styles from './ButtonContainer.module.scss';
import PropTypes from 'prop-types';


const ButtonContainer = ({ favorites, cart }) => {

  const productId = 'some-product-id';

  return (
    <div className={styles.ButtonContainer}>
      <button className={styles.favorite} >

      </button>
      <button className={styles.cart}>

      </button>

    </div>
  );
};

ButtonContainer.propTypes = {
  favorites: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired,
};

export default ButtonContainer;
