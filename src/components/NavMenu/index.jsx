import React from 'react';
import PropTypes from 'prop-types';
import styles from './NavMenu.module.scss';
import { Link } from 'react-router-dom';
import FavoriteIcon from '../svg/FavoriteIcon';
import CartIcon from '../svg/CartIcon';
import HomeIcon from '../svg/HomeIcon';

const NavMenu = ({ favorites = [], cart = [] }) => {

  return (

    <ul className={styles.navContainer}>
      <li>

        <Link to='/' className={styles.home}>
          <HomeIcon />
        </Link>

      </li>

      <li>

        <Link to='/favourite' className={styles.favorite} >
          <FavoriteIcon favorites={favorites} />
          {<span className={styles.favoriteCount}>{favorites.length}</span>}
        </Link>

      </li>

      <li>

        <Link to='/cart' className={styles.cart} ><CartIcon cart={cart} />
          <span className={styles.cartCount}>{cart.length}</span>
        </Link>

      </li>
    </ul>
    
  );
};

NavMenu.propTypes = {
  favorites: PropTypes.array,
  cart: PropTypes.array,
};

export default NavMenu;