import styles from './CartItem.module.scss';
import PropTypes from 'prop-types';

const CartItem = ({ product, onRemoveFromCart }) => {


    return (
                <div className={styles.card}>
                    <div>
                        <h2>{product.productName}</h2>
                        <p>Price: {product.price}</p>
                        <p>article: {product.article}</p>
                    </div>

                    <button onClick={ () => onRemoveFromCart(product) }> Delete </button>
        
                </div>
    );
};

CartItem.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        productName: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        article: PropTypes.string.isRequired,
    }).isRequired,
    onRemoveFromCart: PropTypes.func.isRequired,
};

export default CartItem;
