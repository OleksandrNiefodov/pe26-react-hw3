import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './RenderCard.module.scss';
import FavoriteIcon from '../svg/FavoriteIcon';

const RenderCard = ({ onAddToCart = () => { }, onFavClick, favorites, product }) => {

  const [selectedProduct, setSelectedProduct] = useState(null);

  const toggleModalFavorite = (product) => {
    setSelectedProduct(product);
  };

  return (

    <div className={styles.card}>
      <div className={styles.imageContainer}>
        <img src={product.image} alt="Fruit image" className={styles.image} />

        <FavoriteIcon
          className={styles.cardIcon}
          isFavorite={favorites.includes(product)}
          onClick={() => {
            onFavClick(product);
            toggleModalFavorite(product);
          }}
        />

      </div>
      <div className={styles.cardInfo}>
        <h2>{product.productName}</h2>
        <div className={styles.cardDescriptionContainer}>
          <div className={styles.cardDescription}>
            <p>Color: {product.color}</p>
            <p>Price: {product.price} grn</p>
          </div>
          <div className={styles.cardButtonContainer}>
            <p className={styles.cardArticle}>Article: {product.article}</p>
            <button
              className={styles.cardButton}
              onClick={() => onAddToCart(product)}
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    </div>

  );
};

RenderCard.propTypes = {
  onAddToCart: PropTypes.func,
  onFavClick: PropTypes.func.isRequired,
  favorites: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])).isRequired,
  product: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    image: PropTypes.string.isRequired,
    productName: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    article: PropTypes.string.isRequired,
  }).isRequired,
};

export default RenderCard;
