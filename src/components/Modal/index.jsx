import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss';

const Modal = ({ onConfirm, showModal, onClose, cart, selectedProduct }) => {


  const [isInCart, setIsInCart] = useState(false);

  useEffect(() => {
    setIsInCart(cart.some(item => item.id === selectedProduct?.id));
  }, [cart, selectedProduct]);


  if (!showModal) {
    return false
  }

  return (

    <div className={styles.modalWindow}>

      <div className={styles.content}>
        { isInCart ? <p> This product is already in cart </p> : <p>Do you really want to add this item?</p> }

        <div className={styles.buttonContainer}>
          { isInCart ? (
            <button onClick={onClose}> Got it! </button> 
          ) : (
          <>
          <button onClick={onConfirm} >Yep</button> 
          <button onClick={onClose}>Nope</button> 
          </>
          )}
        </div>


      </div>
      <div className={styles.background} onClick={onClose} >


      </div>
    </div>
  )

};

Modal.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  cart: PropTypes.array.isRequired, 
  selectedProduct: PropTypes.shape({
    id: PropTypes.number.isRequired,
})};

export default Modal;